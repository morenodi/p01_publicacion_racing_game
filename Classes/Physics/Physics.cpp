
#include "Physics.hpp"

bool Physics_CheckCollision(int category_A, int category_B,
  int collision_A, int collision_B) {

  return !((category_A & collision_B) == 0 || (category_B & collision_A) == 0);

}