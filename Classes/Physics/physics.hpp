#pragma once

//Categories

#define CATEGORY_DEFAULT  0x0000
#define CATEGORY_HILLS    0x0001 << 0
#define CATEGORY_WHEEL    0x0001 << 1
#define CATEGORY_BODY     0x0001 << 2
#define CATEGORY_FLAG     0x0001 << 3
#define CATEGORY_COIN     0x0001 << 4
#define CATEGORY_BOT      0x0001 << 5

//Collisions
#define COLLISION_DEFAULT CATEGORY_DEFAULT
#define COLLISION_HILLS   CATEGORY_WHEEL | CATEGORY_BODY | CATEGORY_BOT
#define COLLISION_WHEEL   CATEGORY_HILLS
#define COLLISION_BODY    CATEGORY_HILLS
#define COLLISION_BOT     CATEGORY_HILLS


//Overlaps

#define OVERLAP_DEFAULT   CATEGORY_DEFAULT
#define OVERLAP_HILLS     CATEGORY_WHEEL | CATEGORY_BODY
#define OVERLAP_WHEEL     CATEGORY_FLAG | CATEGORY_COIN | CATEGORY_HILLS
#define OVERLAP_BODY      CATEGORY_FLAG | CATEGORY_COIN | CATEGORY_HILLS
#define OVERLAP_FLAG      CATEGORY_WHEEL | CATEGORY_BODY
#define OVERLAP_COIN      CATEGORY_WHEEL | CATEGORY_BODY
#define OVERLAP_BOT       CATEGORY_FLAG


bool Physics_CheckCollision(int category_A, int category_B,
                            int collision_A, int collision_B);