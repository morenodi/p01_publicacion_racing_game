#include "AppDelegate.h"
#include "MainMenuScene.h"
#include "LevelSelectionScene.h"
#include "RaceScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;


static cocos2d::Size designResolutionSize = cocos2d::Size(1280, 720);

static cocos2d::Size smallResolutionSize = cocos2d::Size(480, 320);
static cocos2d::Size mediumResolutionSize = cocos2d::Size(1024, 768);
static cocos2d::Size largeResolutionSize = cocos2d::Size(2048, 1536);

static cocos2d::Size panoramicResolutionSize = cocos2d::Size(1280, 720);

AppDelegate::AppDelegate() {
  total_stars_ = 0;
}

AppDelegate::~AppDelegate() {
}

// if you want a different context, modify the value of glContextAttrs
// it will affect all platforms
void AppDelegate::initGLContextAttrs() {
  // set OpenGL context attributes: red,green,blue,alpha,depth,stencil
  GLContextAttrs glContextAttrs = { 8, 8, 8, 8, 24, 8 };

  GLView::setGLContextAttrs(glContextAttrs);
}

// if you want to use the package manager to install more packages,  
// don't modify or remove this function
static int register_all_packages() {
  return 0; //flag for packages manager
}

bool AppDelegate::applicationDidFinishLaunching() {
  // initialize director
  auto director = Director::getInstance();
  auto glview = director->getOpenGLView();
  if (!glview) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32) ||  (CC_TARGET_PLATFORM == CC_PLATFORM_MAC) || (CC_TARGET_PLATFORM == CC_PLATFORM_LINUX)
    glview = GLViewImpl::createWithRect("RaceGame",
      cocos2d::Rect(0, 0,
        panoramicResolutionSize.width,
        panoramicResolutionSize.height));
#else
    glview = GLViewImpl::create("RaceGame");
#endif
    director->setOpenGLView(glview);
  }

  // set FPS. the default value is 1.0/60 if you don't call this
  director->setAnimationInterval(1.0f / 60);

  // Set the design resolution
  glview->setDesignResolutionSize(designResolutionSize.width,
    designResolutionSize.height,
    ResolutionPolicy::NO_BORDER);

  auto frameSize = glview->getFrameSize();

  // if the frame's height is larger than the height of medium size.
  /*if (frameSize.height > mediumResolutionSize.height) {
  director->setContentScaleFactor(MIN(panoramicResolutionSize.height / designResolutionSize.height,
  panoramicResolutionSize.width / designResolutionSize.width));
  } else*/ if (frameSize.height > mediumResolutionSize.height) {
  // if the frame's height is larger than the height of medium size.
    director->setContentScaleFactor(MIN(largeResolutionSize.height / designResolutionSize.height,
      largeResolutionSize.width / designResolutionSize.width));
  }
  else if (frameSize.height > smallResolutionSize.height) {
    // if the frame's height is larger than the height of small size.
    director->setContentScaleFactor(MIN(mediumResolutionSize.height / designResolutionSize.height,
      mediumResolutionSize.width / designResolutionSize.width));
  }
  else { // if the frame's height is smaller than the height of medium size.
    director->setContentScaleFactor(MIN(smallResolutionSize.height / designResolutionSize.height,
      smallResolutionSize.width / designResolutionSize.width));
  }


  director->setContentScaleFactor(MIN(panoramicResolutionSize.height / designResolutionSize.height,
    panoramicResolutionSize.width / designResolutionSize.width));


  register_all_packages();

    // create a scene. it's an autorelease object
    auto scene = MainMenuScene::createScene();
//	scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);

    // run
    director->runWithScene(scene);

    // preload audio
    auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
    audio->preloadEffect("pickup.wav");
    audio->preloadEffect("truck_sound.wav");
    audio->preloadEffect("acelarate.wav");
    audio->preloadEffect("engine.wav");
    audio->preloadEffect("brake.wav");
    audio->preloadEffect("click3.wav");
    audio->preloadBackgroundMusic("background.wav");
    audio->setEffectsVolume(0.25f);
    
    return true;
}

// This function will be called when the app is inactive. Note, when receiving a phone call it is invoked.
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be paused
    CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}

void AppDelegate::load_scene(SceneID scene_id, int id) {
  Scene *scene = nullptr;

  switch (scene_id) {
    case kSceneID_Main:
      scene = MainMenuScene::createScene();
      break;
    case kSceneID_LevelSelection:
      scene = LevelSelectionScene::createScene();
      break;
    case kSceneID_Race:
      actual_scene_id_ = id;
      scene = RaceScene::createScene();
      break;
    default:
      assert(false);
      break;
  }
  //Director::getInstance()->replaceScene(TransitionFade::create(1.0f, scene));
  Director::getInstance()->replaceScene(scene);
}

void AppDelegate::add_stars(int number) {
  total_stars_ += number;
}

void AppDelegate::saveOnDisk() {
  FILE *p = nullptr;

  p = fopen("data.dat", "wb");

  if (p != nullptr) {
    fwrite(&data_, sizeof(DataDisk), 1, p);
    fclose(p);
  }

}

void AppDelegate::loadFromDisk() {
  FILE *p = nullptr;

  p = fopen("data.dat", "rb");

  if (p != nullptr) {
    fread(&data_, sizeof(DataDisk), 1, p);
    fclose(p);
  }

}

void AppDelegate::mute() {
  is_muted_ = !is_muted_;

  auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
  if (is_muted_) {
    audio->setBackgroundMusicVolume(0.0f);
    audio->setEffectsVolume(0.0f);
  }
  else {
    audio->setBackgroundMusicVolume(1.0f);
    audio->setEffectsVolume(1.0f);
  }
}

int AppDelegate::get_stars() {
  return total_stars_;
}