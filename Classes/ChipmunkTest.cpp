
#include "ChipmunkTest.h"

USING_NS_CC;


ChipmunkTest::ChipmunkTest() {}

cocos2d::Scene* ChipmunkTest::createScene() {
	// 'scene' is an autorelease object
	auto scene = Scene::createWithPhysics();
	scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);

	// 'layer' is an autorelease object
	auto layer = ChipmunkTest::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

void ChipmunkTest::GenerateCurveFloor (Vec2 origin, Vec2 control1,
                                       Vec2 control2, Vec2 destination,
                                       unsigned int segments) {
	Vec2* vertices = new (std::nothrow) Vec2[segments + 1];
	if (!vertices) return;

	float t = 0;
	for (unsigned int i = 0; i < segments; i++) {
		vertices[i].x = powf(1 - t, 3) * origin.x + 3.0f * powf(1 - t, 2) * t * control1.x + 3.0f * (1 - t) * t * t * control2.x + t * t * t * destination.x;
		vertices[i].y = powf(1 - t, 3) * origin.y + 3.0f * powf(1 - t, 2) * t * control1.y + 3.0f * (1 - t) * t * t * control2.y + t * t * t * destination.y;
		t += 1.0f / segments;
	}
	vertices[segments].x = destination.x;
	vertices[segments].y = destination.y;

	auto physics_body = PhysicsBody::createEdgeChain(vertices, segments+1);
	physics_body->setDynamic(false);

	auto col = Node::create();
	col->setPosition(Vec2(0.0f, 0.0f));
	col->addComponent(physics_body);
	addChild(col);
	
	CC_SAFE_DELETE_ARRAY(vertices);
}

bool ChipmunkTest::init() {

	return true;
}
