#include "AppDelegate.h"
#include "RaceScene.h"
#include "SimpleAudioEngine.h"
#include "Random/Random.h"
#include "Car/car.h"

#include "ui/CocosGUI.h"
#include "SimpleAudioEngine.h"

#include "Physics/physics.hpp"

USING_NS_CC;

// macros random
#define RANDOM(X) Random::next()%(int)X
#define RANDOM_SEED(X) Random::setSeed(X)

RaceScene::RaceScene() {
  HUD_ = nullptr;
  pause_menu_ = nullptr;
  myCamera_ = nullptr;
  world_ = nullptr;
  hills_ = nullptr;
  car_ = nullptr;

  //Timer
  timer_ = 0.0f;//seconds
  counter_ = 1.0f;

  params_.time = 60.0f;
  params_.coins = 4;
  params_.position = 1;
  params_.total_players = 4;

  stars_ = 0;

  coins_ = 0;
  position_ = 1;

  acelerate_ = false;
  brake_ = false;

  race_ended_ = false;
  is_paused_ = false;

  truck_sound_effect_id_ = 0;
  first_decelerate_ = true;
  first_acelerate_ = true;
}

RaceScene::~RaceScene() {

  auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
  //audio->stopBackgroundMusic("background.wav");
  audio->stopAllEffects();
}

Scene* RaceScene::createScene() {

  // 'scene' is an autorelease object
  auto scene = Scene::createWithPhysics();

  // 'layer' is an autorelease object
  auto layer = RaceScene::create();

  // add layer as a child to scene
  scene->addChild(layer, 0, "MainLayer");

  scene->addChild(new cocos2d::Layer(), 0, "HUD");

  // return the scene
  return scene;
}

// on "init" you need to initialize your instance
bool RaceScene::init() {
  scene_id_ = AppDelegate::instance().getActualId();
  RANDOM_SEED(513561313654643 + scene_id_);
  //////////////////////////////
  // 1. super init first
  if (!Layer::init()) {
    return false;
  }

  this->schedule(schedule_selector(RaceScene::update_countdown));

  myCamera_ = Director::getInstance()->getRunningScene()->getDefaultCamera();
  //addChild(myCamera_, 0);

  visibleSize_ = Director::getInstance()->getVisibleSize();
  origin_ = Director::getInstance()->getVisibleOrigin();

  generate_terrain();
  generate_parallax();

  // enables physics collision
  auto contactListener = EventListenerPhysicsContact::create();
  contactListener->onContactBegin = CC_CALLBACK_1(RaceScene::onContactBegin, this);
  _eventDispatcher->addEventListenerWithSceneGraphPriority(contactListener, this);

  auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
  audio->pauseBackgroundMusic();
  audio->playEffect("background.wav", true, 1.0f, 1.0f, 1.0f);
  truck_sound_effect_id_ = audio->playEffect("engine.wav", true, 1.0f, 1.0f, 1.0f);

  return true;
}

void RaceScene::update(float delta_time) {
  if (!is_paused_) {
    if (!race_ended_) update_timer(delta_time);
    update_car(delta_time);
    update_camera(delta_time);

    for (int i = 0; i < params_.total_players - 1; ++i) {
      update_bot(bots_[i]);
    }

  }

}

void RaceScene::menuCloseCallback(Ref* pSender) {
  //Close the cocos2d-x game scene and quit the application
  Director::getInstance()->end();
  //auto chipmunk_scene = ChipmunkTest::createScene();
  //Director::getInstance()->replaceScene(chipmunk_scene);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
  exit(0);
#endif

  /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/

  //EventCustom customEndEvent("game_scene_close_event");
  //_eventDispatcher->dispatchEvent(&customEndEvent);
}

bool RaceScene::onTouchBegan(Touch* touch, Event* event) {
  //labelTouchInfo_->setPosition(touch->getLocation());


  //labelTouchInfo_->setString("You Touched Here");
  return true;
}

void RaceScene::onTouchEnded(Touch* touch, Event* event) {

}

void RaceScene::onEnter() {
  Layer::onEnter();

  visibleSize_ = Director::getInstance()->getVisibleSize();
  origin_ = Director::getInstance()->getVisibleOrigin();

  world_ = Director::getInstance()->getRunningScene()->getPhysicsWorld();
  HUD_ = Director::getInstance()->getRunningScene()->getChildByName("HUD");

  generate_car();
  generate_bots();

  create_HUD();

}

void RaceScene::set_level_params(LevelParams *params) {
  params_.time = params->time;
  params_.position = params->position;
  params_.coins = params->coins;
  params_.total_players = params->total_players;
}

void RaceScene::generate_terrain() {
  hills_ = Hills::create();
  //hills_->createHills(Point(origin_.x, 150.0f), 100, 150.0f, 500.0f, 100.0f, 5);
  hills_->createHills(Point(origin_.x, 150.0f), 100, 150.0f, 25.0f * (scene_id_ + 1), visibleSize_.width*0.15f, 5);
  hills_->generateCoins(50, "coin.png", visibleSize_.width * 0.05f);
  this->addChild(hills_, 0, "Hills");
}

void RaceScene::generate_parallax() {
  parallax_ = Parallax::create();
  parallax_->init_parallax(visibleSize_.width, Vec2(origin_.x, origin_.y - visibleSize_.height * 0.5f));
  this->addChild(parallax_, -1, "Parallax");
}

void RaceScene::create_HUD() {
  visibleSize_ = Director::getInstance()->getVisibleSize();
  origin_ = Director::getInstance()->getVisibleOrigin();
  char buffer[20];

  auto bAccelerate = ui::Button::create("gasandbrake.png", "gasandbrake.png");
  bAccelerate->setPosition(Point(visibleSize_.width - 70.0f, 40.0f));
  bAccelerate->setScale(0.3);
  bAccelerate->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
    switch (type)
    {
    case ui::Widget::TouchEventType::BEGAN:
      acelerate_ = true;
      break;
    case ui::Widget::TouchEventType::ENDED:
      acelerate_ = false;
      break;
    default:
      break;
    }
  });
  HUD_->addChild(bAccelerate);

  auto bBrake = ui::Button::create("gasandbrake.png", "gasandbrake.png");
  bBrake->setPosition(Point(70.0f, 40.0f));
  bBrake->setScale(0.3);
  bBrake->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
    switch (type)
    {
    case ui::Widget::TouchEventType::BEGAN:
      brake_ = true;
      break;
    case ui::Widget::TouchEventType::ENDED:
      brake_ = false;
      break;
    default:
      break;
    }
  });
  HUD_->addChild(bBrake);

  char pos_text[20] = "";
  memset(buffer, 0, 20);
  sprintf(pos_text, "%d", position_);
  strcat(pos_text, "/");
  memset(buffer, 0, 20);
  sprintf(buffer, "%d", params_.total_players);
  strcat(pos_text, buffer);
  auto tPosition = Label::create(pos_text, "fonts/SF Comic Script.ttf", 40);
  tPosition->setTextColor(Color4B(0, 0, 0, 255));
  tPosition->setPosition(Point(100.0f, visibleSize_.height - 30.0f));
  tPosition->setHorizontalAlignment(kCCTextAlignmentLeft);
  HUD_->addChild(tPosition, 1, "Position");

  auto sPosition = Sprite::create("end_flag.png");
  sPosition->setPosition(Point(30.0f, visibleSize_.height - 30.0f));
  sPosition->setScale(0.15f);
  sPosition->setRotation3D(Vec3(15.0f, 180.0f, 20.0f));
  HUD_->addChild(sPosition);

  char coins_text[20] = "";
  sprintf(coins_text, "%d", coins_);
  auto tCoins = Label::create(coins_text, "fonts/SF Comic Script.ttf", 40);
  tCoins->setTextColor(Color4B(0, 0, 0, 255));
  tCoins->setPosition(Point(80.0f, visibleSize_.height - 80.0f));
  tCoins->setHorizontalAlignment(kCCTextAlignmentLeft);
  HUD_->addChild(tCoins, 1, "Coins");

  auto sCoins = Sprite::create("coin.png");
  sCoins->setPosition(Point(35.0f, visibleSize_.height - 80.0f));
  sCoins->setScale(0.15f);
  HUD_->addChild(sCoins);

  auto tTimer = Label::create("", "fonts/SF Comic Script.ttf", 40);
  tTimer->setTextColor(Color4B(0, 0, 0, 255));
  tTimer->setPosition(Point(visibleSize_.width / 2 + origin_.x, visibleSize_.height - 30.0f));
  HUD_->addChild(tTimer, 1, "Timer");

  auto bPause = ui::Button::create("UI/pause.png", "UI/pause.png");
  bPause->setPosition(Point(visibleSize_.width - 50.0f, visibleSize_.height - 50.0f));
  bPause->setScale(0.1f);
  bPause->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
    switch (type)
    {
    case ui::Widget::TouchEventType::BEGAN:
      break;
    case ui::Widget::TouchEventType::ENDED:
      pause();
      break;
    default:
      break;
    }
  });
  HUD_->addChild(bPause);

  create_pause();
}

void RaceScene::create_pause() {

  // Pause Menu
  pause_menu_ = Node::create();
  pause_menu_->setAnchorPoint(Vec2(0.0f, 0.0f));
  auto bgAlpha = Sprite::create("UI/AlphaGrey.png");
  bgAlpha->setAnchorPoint(Point(0, 0));
  pause_menu_->addChild(bgAlpha);

  auto woodPanel = Sprite::create("UI/WoodPanel.png");
  woodPanel->setPosition(Point(visibleSize_.width / 2 + origin_.x, visibleSize_.height / 2 + origin_.y));
  //woodPanel->setScale(0.5f, 1.0f);
  pause_menu_->addChild(woodPanel);

  auto bResume = ui::Button::create("UI/tResume.png", "UI/tResume_s.png");
  bResume->setPosition(Point(visibleSize_.width / 2 + origin_.x, visibleSize_.height / 2 + origin_.y - 0.0f));
  bResume->setScale(0.75f);
  bResume->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
    switch (type) {
    case ui::Widget::TouchEventType::BEGAN:

      break;
    case ui::Widget::TouchEventType::ENDED:
      pause();

      break;
    default:
      break;
    }
  });
  pause_menu_->addChild(bResume);

  auto bQuitGame = ui::Button::create("UI/tQuitGame.png", "UI/tQuitGame_s.png");
  bQuitGame->setPosition(Point(visibleSize_.width / 2 + origin_.x, visibleSize_.height / 2 + origin_.y - 140.0f));
  bQuitGame->setScale(0.75f);
  bQuitGame->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
    switch (type) {
    case ui::Widget::TouchEventType::BEGAN:

      break;
    case ui::Widget::TouchEventType::ENDED:
      AppDelegate::instance().load_scene(SceneID::kSceneID_Main);
      break;
    default:
      break;
    }
  });
  pause_menu_->addChild(bQuitGame);

  auto soundDisabled = ui::CheckBox::create("UI/soundYes.png", "UI/soundNo.png");
  soundDisabled->setPosition(Point(visibleSize_.width - 45, 40));
  soundDisabled->setScale(0.1f);
  soundDisabled->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
    switch (type) {
    case ui::Widget::TouchEventType::BEGAN:
      //DO
      break;
    case ui::Widget::TouchEventType::ENDED:

      break;
    default:
      break;
    }
  });

  pause_menu_->addChild(soundDisabled);
  this->addChild(pause_menu_, 10);
  is_paused_ = false;

  pause_menu_->setVisible(is_paused_);
}

void RaceScene::generate_car() {
  car_ = Car::create();
  // joint
  if (world_ == nullptr) {
    world_ = Director::getInstance()->getRunningScene()->getPhysicsWorld();
  }
  car_->createJoints(world_);
  this->addChild(car_, 1, "Car");

  car_->setScale(0.25f);
  car_->setPosition(origin_ + Vec2(visibleSize_.width / 2,
    visibleSize_.height / 2 + 100.0f));
}

void RaceScene::generate_bots() {
  Car *bot = nullptr;
  char buffer[20] = "";

  bots_.reserve(params_.total_players - 1);

  for (int i = 0; i < params_.total_players - 1; ++i) {
    bot = Car::create();
    // joint
    if (world_ == nullptr) {
      world_ = Director::getInstance()->getRunningScene()->getPhysicsWorld();
    }
    bot->createJoints(world_);
    bots_.push_back(bot);

    memset(buffer, 0, 20);
    strcat(buffer, "Bot");
    sprintf(buffer, "%d", i);

    this->addChild(bot, 1, buffer);// Bots
    bot->setScale(0.25f);
    bot->setPosition(origin_ +
      Vec2(visibleSize_.width * (i + 0.5f),
        visibleSize_.height / 2 + 100.0f));
    bot->enable_bot_collisions();
  }

}

void RaceScene::update_countdown(float delta_time) {
  Label *timer_label = (Label *)HUD_->getChildByName("Timer");
  char buffer[10];

  update_camera(delta_time);

  counter_ -= delta_time;

  if (counter_ <= 0.0f) {
    strcpy(buffer, "GO!");
    this->unschedule(schedule_selector(RaceScene::update_countdown));
    this->schedule(schedule_selector(RaceScene::update));
  }
  else {
    sprintf(buffer, "%.0f", counter_ + 1);
  }

  timer_label->setString(buffer);
}

void RaceScene::update_car(float delta_time) {
  //Update Car velocity
  auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
  car_->decelerate();
  if (acelerate_) {
    if (first_acelerate_) {
      audio->playEffect("acelerate.wav", false, 1.0f, 1.0f, 1.0f);
    }
    car_->acelerate();
    first_acelerate_ = false;
  }
  else {
    //audio->stopEffect(truck_sound_effect_id_);

    first_acelerate_ = true;
  }
  if (brake_) {
    car_->brake();

    if (first_decelerate_) {
      audio->playEffect("brake.wav", false, 1.0f, 1.0f, 1.0f);
    }
    first_decelerate_ = false;
  }
  else {
    first_decelerate_ = true;
  }

  if (car_->get_velocity().x < 2.0f) {
    audio->pauseEffect(truck_sound_effect_id_);
  }
  else {
    audio->resumeEffect(truck_sound_effect_id_);
  }

  check_position();
}

void RaceScene::update_bot(Car *car) {

  ContactState state = car->get_current_contact_state();
  float angle = car->get_rotation();
  angle += 360.0f;
  angle = (int)angle % 360;
  angle *= -1.0f;

  //car->decelerate();

  /*if (state == kContactState_Floor || state == kContactState_TouchingForward) {
  car->acelerate();
  } else if (state == kContactState_TouchingBack) {
  car->brake();
  } else if (state == kContactState_NoTouch) {*/
  if (state == kContactState_Floor ||
    (state == kContactState_TouchingForward && angle < 45.0f) ||
    (state == kContactState_NoTouch && angle > 180.0f)) {
    car->acelerate();
  }

  if (state == kContactState_TouchingBack && angle > 45.0f ||
    (state == kContactState_NoTouch && angle <= 180.0f)) {
    car->brake();
  }
}

void RaceScene::update_camera(float delta_time) {
  //Update camera and HUD position relative to car position
  myCamera_ = Director::getInstance()->getRunningScene()->getDefaultCamera();
  Vec2 car_position = car_->get_position();
  Vec2 new_position = Vec2(car_position.x + visibleSize_.width * 0.1f,
    car_position.y + visibleSize_.height * 0.15f);
  myCamera_->setPosition(new_position);
  HUD_->setPosition(Vec2(car_position.x - visibleSize_.width * 0.4f,
    car_position.y - visibleSize_.height * 0.35f));

  int direction = (car_->get_velocity().x >= 0) ? 1 : -1;

  parallax_->update_parallax(new_position.x, direction);
}

void RaceScene::update_timer(float delta_time) {
  Label *timer_label = (Label *)HUD_->getChildByName("Timer");
  char buffer[10];

  timer_ += delta_time;

  sprintf(buffer, "%05.2f", timer_);

  timer_label->setString(buffer);
}

void RaceScene::update_coins() {
  Label *coin_label = (Label *)HUD_->getChildByName("Coins");
  char coins_text[20] = "";

  coins_++;

  sprintf(coins_text, "%d", coins_);
  strcat(coins_text, " coins");

  coin_label->setString(coins_text);
}

void RaceScene::update_position() {
  Label *position_label = (Label *)HUD_->getChildByName("Position");
  char pos_text[20] = "";
  char buffer[20] = "";

  sprintf(pos_text, "%d", position_);
  strcat(pos_text, "/");
  sprintf(buffer, "%d", params_.total_players);
  strcat(pos_text, buffer);

  position_label->setString(pos_text);
}

void RaceScene::check_position() {
  Vec2 car_position = car_->get_position();
  int position = 1;
  for (int i = 0; i < params_.total_players - 1; ++i) {
    Car *bot = bots_[i];
    if (car_position.x < bot->get_position().x) {
      position++;
    }
  }
  if (position != position_) {
    position_ = position;
    update_position();
  }
}

void RaceScene::end_race() {
  if (race_ended_) return;
  race_ended_ = true;

  stars_ = 0;

  if (timer_ <= params_.time) stars_++;
  if (position_ <= params_.position) stars_++;
  if (coins_ >= params_.coins) stars_++;

  // test
  Label *timer_label = (Label *)HUD_->getChildByName("Timer");
  char buffer[10];

  sprintf(buffer, "%d", stars_);
  strcat(buffer, " stars");
  timer_label->setString(buffer);

  AppDelegate::instance().add_stars(stars_);

  pause();

}

void RaceScene::pause() {
  Vec2 camera_pos = myCamera_->getPosition();

  pause_menu_->setPosition(camera_pos.x - visibleSize_.width*0.5f,
    camera_pos.y - visibleSize_.height*0.5f);

  is_paused_ = !is_paused_;

  pause_menu_->setVisible(is_paused_);
  //world_->getUpdateRate();
  world_->setAutoStep(!is_paused_);

  // TODO: disable/enable car physics
}

void RaceScene::load_scene(SceneID scene) {
  AppDelegate::instance().load_scene(scene);
}

void RaceScene::coin_tweening(Node *node) {

  Label *coin_label = (Label *)HUD_->getChildByName("Coins");
  MoveBy *moveby = MoveBy::create(0.5f, Point(0.0f, 50.0f));
  EaseBackOut *ease = EaseBackOut::create(moveby->clone());
  FadeOut *fadeout = FadeOut::create(0.5f);
  node->runAction(ease);
  node->runAction(fadeout);

  auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
  audio->playEffect("pickup.wav", false, 1.0f, 1.0f, 1.0f);
}

bool RaceScene::onContactBegin(PhysicsContact& contact) {
  auto bodyA = contact.getShapeA()->getBody();
  auto bodyB = contact.getShapeB()->getBody();

  //Coin picked
  if (bodyA->getCategoryBitmask() == CATEGORY_COIN) {
    update_coins();
    bodyA->setContactTestBitmask(OVERLAP_DEFAULT);
    coin_tweening(bodyA->getOwner());
  }
  else if (bodyB->getCategoryBitmask() == CATEGORY_COIN) {
    update_coins();
    bodyB->setContactTestBitmask(OVERLAP_DEFAULT);
    coin_tweening(bodyB->getOwner());
  }

  //Flag => End Race
  if (bodyA->getCategoryBitmask() == CATEGORY_FLAG) {
    end_race();
    bodyB->setContactTestBitmask(OVERLAP_DEFAULT);
  }
  else if (bodyB->getCategoryBitmask() == CATEGORY_FLAG) {
    end_race();
    bodyA->setContactTestBitmask(OVERLAP_DEFAULT);
  }


  return true;
}