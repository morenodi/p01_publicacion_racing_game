#ifndef __CHIPMUNKTEST_SCENE_H__
#define __CHIPMUNKTEST_SCENE_H__

#include "cocos2d.h"
//#include "chipmunk.h"
#include "VisibleRect.h"

USING_NS_CC;

class ChipmunkTest : public Layer
{
public:
	CREATE_FUNC(ChipmunkTest);

	static cocos2d::Scene* createScene();

	virtual bool init();

	void GenerateCurveFloor(cocos2d::Vec2 origin, cocos2d::Vec2 control1,
                          cocos2d::Vec2 control2, cocos2d::Vec2 destination,
                          unsigned int segments);

	ChipmunkTest();
	//~ChipmunkTest();

};


#endif
