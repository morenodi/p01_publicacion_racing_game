#include "parallax.h"

Parallax::Parallax() {
	height_image_ = 0.0f;
	last_x_pos_ = 0.0f;
	width_image_ = 0.0f;
}

void Parallax::init_parallax(float width, Vec2 origin) {
  auto sp1 = Sprite::create("back_p1.png");
  float ratio = sp1->getContentSize().width / sp1->getContentSize().height;
  height_image_ = width * ratio;
  width_image_ = width;
	
  Texture2D::TexParams params = { GL_LINEAR,GL_LINEAR,GL_REPEAT,GL_REPEAT };

  sp1->setAnchorPoint(Vec2(0.5, 0.5));
  sp1->setScale(100.0f);
  sp1->setPosition(Vec2((width) * -2, 0));
  sp1->setContentSize(Size(width, height_image_));

  auto node1 = Node::create();

  for (int i = -1; i < 2; i++) {
    auto sp = Sprite::create("layer1_p1.png");
    sp->setAnchorPoint(Vec2(0, 0));
    sp->setPosition(Vec2((width - 1) * i, 0));
    sp->setContentSize(Size(width, height_image_));
    sp->getTexture()->setTexParameters(params);
    node1->addChild(sp);
  }
	

  auto node2 = Node::create();

  for (int i = -1; i < 2; i++) {
    auto sp = Sprite::create("layer2_p1.png");
    sp->setAnchorPoint(Vec2(0, 0));
    sp->setPosition(Vec2((width - 1) * i, 0));
    sp->setContentSize(Size(width, height_image_));
    sp->getTexture()->setTexParameters(params);
    node2->addChild(sp);
  }

  auto node3 = Node::create();

  for (int i = -1; i < 2; i++) {
    auto sp = Sprite::create("layer3_p1.png");
    sp->setAnchorPoint(Vec2(0, 0));
    sp->setPosition(Vec2((width - 1) * i, 0));
    sp->setContentSize(Size(width, height_image_));
    sp->getTexture()->setTexParameters(params);
    node3->addChild(sp);
  }

  auto node4 = Node::create();

  for (int i = -1; i < 2; i++) {
    auto sp = Sprite::create("front_p1.png");
    sp->setAnchorPoint(Vec2(0, 0));
    sp->setPosition(Vec2((width - 1) * i, 0));
    sp->setContentSize(Size(width, height_image_));
    sp->getTexture()->setTexParameters(params);
    node4->addChild(sp);
  }

  addChild(sp1, -6, Vec2(0.0f, 0.0f), origin);
  addChild(node1, -5, Vec2(0.05f, 0.0f), origin);
  addChild(node2, -4, Vec2(0.1f, 0.0f), origin);
  addChild(node3, -3, Vec2(0.2f, 0.0f), origin);
  addChild(node4, -2, Vec2(0.4f, 0.0f), origin);

}

void Parallax::update_parallax(float position, int direction) {
  Vector<Node*> childs = getChildren();

  auto visibleSize = Director::getInstance()->getVisibleSize();

  Vec2 origin = Director::getInstance()->getVisibleOrigin();

  for (int i = 0; i < getChildrenCount(); ++i) {
    Node* child = childs.at(i);

    Vector<Node*> layers = child->getChildren();
    for (int j = 0; j < child->getChildrenCount(); ++j) {
      Node* layer = childs.at(i);
      Vec2 my_position = layer->getPosition();

      int diff = position - my_position.x;

      if (diff > visibleSize.width * 1.5f || diff < visibleSize.width * -0.5f) {
        layer->setPosition(my_position.x + direction * 2 * visibleSize.width, my_position.y);

      }
    }
  }
}