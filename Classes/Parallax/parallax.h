#pragma once

#include "cocos2d.h"

USING_NS_CC;

class Parallax : public ParallaxNode
{
public:
	Parallax();
	~Parallax() {}

	void init_parallax(float width, Vec2 origin);
 void update_parallax(float position, int direction);
	
	CREATE_FUNC(Parallax);

private:
	float height_image_, width_image_;
	float last_x_pos_;

};