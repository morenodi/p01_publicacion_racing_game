#ifndef __MAIN_MENU_SCENE_H__
#define __MAIN_MENU_SCENE_H__

#include "cocos2d.h"

#include "AppDelegate.h"

class MainMenuScene : public cocos2d::Layer {
 public:
  static cocos2d::Scene* createScene();

  virtual bool init();

  // implement the "static create()" method manually
  CREATE_FUNC(MainMenuScene);

  void update(float dt);

  void playClickSound();

 private:
  void load_scene(SceneID scene);

  cocos2d::Sprite* background1;
  cocos2d::Sprite* background2;
  cocos2d::Sprite* background3;

};

#endif // __MAIN_MENU_SCENE_H__
