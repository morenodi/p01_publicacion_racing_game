#ifndef __LEVEL_SELECTION_SCENE_H__
#define __LEVEL_SELECTION_SCENE_H__

#include "cocos2d.h"

#include "AppDelegate.h"

class LevelSelectionScene : public cocos2d::Layer {
 public:
  static cocos2d::Scene* createScene();

  virtual bool init();

  // implement the "static create()" method manually
  CREATE_FUNC(LevelSelectionScene);

  void update(float dt);
  void load_scene(SceneID scene);

  void playClickSound();

 private:

  cocos2d::Sprite* background1;
  cocos2d::Sprite* background2;
  cocos2d::Sprite* background3;
};

#endif // __LEVEL_SELECTION_SCENE_H__
