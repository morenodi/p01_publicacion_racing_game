#include "RaceScene.h"
#include "SimpleAudioEngine.h"
#include "car.h"

#include "Physics/physics.hpp"

USING_NS_CC;

Car::Car(){
  maxVelocity_ = 100000.0f;
  baseForce_ = 25 * 1000.0f;

  rotation_force_ = 2 * 1000.0f;

  head_ = nullptr;
  wheel_ = nullptr;
  wheel2_ = nullptr;
  body_ = nullptr;

  floor_ = nullptr;

  right_wheel_touching_ = false;
  left_wheel_touching_ = false;

}

Car::~Car(){
}

bool Car::init() {

	if (!Node::init()) {
		return false;
	}

 
	// body
	auto my_body = createBodyWork(1.0f, Vec2(0.0f, 0.0f));
 body_ = my_body->getPhysicsBody();
 body_->setMass(10.0f);
 //body_->setVelocityLimit(maxVelocity_);
 //body_->setAngularVelocityLimit(1.0f);
	addChild(my_body);

	// wheels
	auto my_wheel = createWheel(1.0f, Vec2(-75.0f * 2.6f, -50.0f * 2.25f));
	auto my_wheel2 = createWheel(1.0f, Vec2(75.0f * 2.6f, -50.0f * 2.25f));
 wheel_ = my_wheel->getPhysicsBody();
 wheel2_ = my_wheel2->getPhysicsBody();
 wheel_->setName(std::string("left_wheel"));
 wheel2_->setName(std::string("right_wheel"));
 addChild(my_wheel);
 addChild(my_wheel2);

	// head
 //-10.0f, my_body->getContentSize().height / 2
	auto my_head = createHead(1.0f, Vec2(my_body->getContentSize().width*0.4f, 
                                      my_body->getContentSize().height));
 
 //head_ = my_head->getPhysicsBody();
 
 //head_->setCategoryBitmask(0x01);
	//head_->setCollisionBitmask(0x00);
	

 ///////////
 //head_->setEnabled(false);

 my_body->addChild(my_head);

 // enables physics collision
 auto contactListener = EventListenerPhysicsContact::create();
 contactListener->onContactBegin = CC_CALLBACK_1(Car::onContactBegin, this);
 contactListener->onContactSeparate = CC_CALLBACK_1(Car::onContactSeparate, this);
 _eventDispatcher->addEventListenerWithSceneGraphPriority(contactListener, this);

 return true;

}

Sprite *Car::createWheel(float scale, Vec2 relative_pos) {
  cocos2d::Sprite* wheel = cocos2d::Sprite::create("truck/Wheel.png");

  // Modified: define the relative position from the center of the car
  wheel->setPosition(relative_pos);
  // Modified: set the scale by parameter
  wheel->setScale(scale);

  // Modified: Size of half the sprite, instead of constants
  cocos2d::PhysicsBody* physics_body = cocos2d::PhysicsBody::createCircle(wheel->getContentSize().width / 2,
                                                                          cocos2d::PhysicsMaterial(0.1f, 0.0f, 0.0f));

  physics_body->setCategoryBitmask(CATEGORY_WHEEL);
  physics_body->setCollisionBitmask(COLLISION_WHEEL);
  physics_body->setContactTestBitmask(OVERLAP_WHEEL);
  wheel->setPhysicsBody(physics_body);


	return wheel;
}

Sprite *Car::createHead(float scale, Vec2 relative_pos) {
	cocos2d::Sprite* head = cocos2d::Sprite::create("truck/Head.png");

	// Modified: define the relative position from the center of the car
	head->setPosition(relative_pos + Vec2(-10.0f * 2.6f, 45.0f * 2.25f));
	// Modified: set the scale by parameter
	head->setScale(scale);

	// Modified: set origin
	//head->setAnchorPoint(Vec2(0.5f, 1.0f));

	// Modified: Size of half the sprite, instead of constants
 /*cocos2d::PhysicsBody* physics_body = cocos2d::PhysicsBody::createBox(head->getContentSize(),
                                                                      cocos2d::PhysicsMaterial(1.0f, 0.0f, 1.0f));
	head->setPhysicsBody(physics_body);*/

	return head;
}

Sprite *Car::createBodyWork(float scale, Vec2 relative_pos) {
	cocos2d::Sprite* bodyWork = Sprite::create("truck/Body.png");
	
	// Modified: define the relative position from the center of the car
	bodyWork->setPosition(relative_pos);
	// Modified: set the scale by parameter
	bodyWork->setScale(scale);

	// Modified: Size of the sprite * 1.1f, instead of constants
 cocos2d::PhysicsBody* physics_body = cocos2d::PhysicsBody::createBox(bodyWork->getContentSize() * 0.9f,
                                                 cocos2d::PhysicsMaterial(0.1f, 0.5f, 0.0f));

 physics_body->setCategoryBitmask(CATEGORY_BODY);
 physics_body->setCollisionBitmask(COLLISION_BODY);
 physics_body->setContactTestBitmask(OVERLAP_BODY);

 //physics_body->setAngularDamping(1.0f);

	bodyWork->setPhysicsBody(physics_body);
	
	return bodyWork;
}

void Car::createJoints(PhysicsWorld *world_) {
  float ratio_x = 2.6f;
  float ratio_y = 2.25f;

  //Joint between wheels
  PhysicsJointLimit* joint = PhysicsJointLimit::construct(wheel_, wheel2_, Point::ZERO,
                                                          Point::ZERO, 50.0f * ratio_x, 50.0f * ratio_x);

	PhysicsJointLimit* joint_limit = PhysicsJointLimit::construct(wheel_, body_, Point::ZERO,
                                                               Vec2(-25.0f * ratio_x, -20.0f * ratio_y),
                                                                0.0f, 0.1f);

	PhysicsJointLimit* joint_limit2 = PhysicsJointLimit::construct(wheel2_, body_, Point::ZERO,
                                                                Vec2(25.0f * ratio_x, -20.0f * ratio_y),
                                                                0.0f, 0.1f);
	/*PhysicsJointLimit* jointSpring3 = PhysicsJointLimit::construct(head_, body_, Point::ZERO,
                                                                Vec2(5.0f, 50.0f),
                                                                0.0f, 0.0f);*/
	world_->addJoint(joint);
	world_->addJoint(joint_limit);
	world_->addJoint(joint_limit2);
	//world_->addJoint(jointSpring3);
}

void Car::acelerate() {
  
  body_->applyForce(Vec2(baseForce_, 0));
  if (!right_wheel_touching_) {
    body_->applyForce(Vec2(0.0f, rotation_force_), Vec2(10.0f, -10.0f));
  }

  /*if (right_wheel_touching_ || left_wheel_touching_) {
  }*/    


  wheel_->setAngularVelocity(-get_velocity().x);
  wheel2_->setAngularVelocity(-get_velocity().x);
}

void Car::decelerate() {

    body_->applyForce(Vec2(1000.0f, 0.0f));
  if (get_velocity().x>0)
    body_->applyForce(Vec2(-1000.0f, 0.0f));
  /*if (get_velocity().x < 0)*/
  wheel_->setAngularVelocity(-get_velocity().x);
  wheel2_->setAngularVelocity(-get_velocity().x);
}

void Car::brake() {
  
  if (!left_wheel_touching_) {
    body_->applyForce(Vec2(0.0f, -rotation_force_), Vec2(10.0f, 10.0f));
  }
  if (right_wheel_touching_ || left_wheel_touching_) {
    body_->applyForce(Vec2(-baseForce_, 0));
  }

  wheel_->setAngularVelocity(-get_velocity().x);
  wheel2_->setAngularVelocity(-get_velocity().x);
}

Vec2 Car::get_position() {

  return body_->getPosition();

}

Vec2 Car::get_velocity() {
  return body_->getVelocity();

}

float Car::get_rotation() {
  return body_->getRotation();
}

ContactState Car::get_current_contact_state() {
  ContactState state = kContactState_Floor;

  if (right_wheel_touching_ && left_wheel_touching_) {
    state = kContactState_Floor;
  }
  else if (right_wheel_touching_ && !left_wheel_touching_) {
    state = kContactState_TouchingForward;
  }
  else if (!right_wheel_touching_ && left_wheel_touching_) {
    state = kContactState_TouchingBack;
  }
  else if (!right_wheel_touching_ && !left_wheel_touching_) {
    state = kContactState_NoTouch;
  }

  return state;
}

void Car::enable_bot_collisions() {

  body_->setCategoryBitmask(CATEGORY_BOT);
  body_->setCollisionBitmask(COLLISION_BOT);
  body_->setContactTestBitmask(OVERLAP_BOT);

  wheel_->setCategoryBitmask(CATEGORY_BOT);
  wheel_->setCollisionBitmask(COLLISION_BOT);
  wheel_->setContactTestBitmask(OVERLAP_BOT);

  wheel2_->setCategoryBitmask(CATEGORY_BOT);
  wheel2_->setCollisionBitmask(COLLISION_BOT);
  wheel2_->setContactTestBitmask(OVERLAP_BOT);

}

bool Car::onContactBegin(PhysicsContact& contact) {
  auto bodyA = contact.getShapeA()->getBody();
  auto bodyB = contact.getShapeB()->getBody();

  std::string nameA = bodyA->getName();
  std::string nameB = bodyB->getName();

  if (bodyA->getCategoryBitmask() == CATEGORY_WHEEL && bodyB->getCategoryBitmask() == CATEGORY_HILLS ||
      bodyB->getCategoryBitmask() == CATEGORY_WHEEL && bodyA->getCategoryBitmask() == CATEGORY_HILLS) {
    
    if (nameA == "left_wheel" || nameB == "left_wheel") {
      left_wheel_touching_ = true;
    } else if (nameA == "right_wheel" || nameB == "left_wheel") {
      right_wheel_touching_ = true;
    }
  }



  return true;
}

void Car::onContactSeparate(PhysicsContact& contact) {
  auto bodyA = contact.getShapeA()->getBody();
  auto bodyB = contact.getShapeB()->getBody();

  std::string nameA = bodyA->getName();
  std::string nameB = bodyB->getName();

  if (bodyA->getCategoryBitmask() == CATEGORY_WHEEL && bodyB->getCategoryBitmask() == CATEGORY_HILLS ||
    bodyB->getCategoryBitmask() == CATEGORY_WHEEL && bodyA->getCategoryBitmask() == CATEGORY_HILLS) {

    if (nameA == "left_wheel" || nameB == "left_wheel") {
      left_wheel_touching_ = false;
    }
    else if (nameA == "right_wheel" || nameB == "left_wheel") {
      right_wheel_touching_ = false;
    }
  }
}