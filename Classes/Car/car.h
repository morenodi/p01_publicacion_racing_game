#ifndef __CAR_H__
#define __CAR_H__

#include "cocos2d.h"

USING_NS_CC;

typedef enum {
  kContactState_Floor = 0,
  kContactState_TouchingBack,
  kContactState_TouchingForward,
  kContactState_NoTouch
}ContactState;


class Car : public Node {
 public:
  Car();
  ~Car();
  bool init();

  Sprite *createWheel(float scale, Vec2 relative_pos);
  Sprite *createHead(float scale, Vec2 relative_pos);
  Sprite *createBodyWork(float scale, Vec2 relative_pos);

  void createJoints(PhysicsWorld *world_);

  void acelerate();
  void decelerate();
  void brake();
	
  CREATE_FUNC(Car);

  Vec2 get_position();
  Vec2 get_velocity();
  float get_rotation();
  ContactState get_current_contact_state();

  void enable_bot_collisions();

  bool onContactBegin(PhysicsContact& contact);
  void onContactSeparate(PhysicsContact& contact);

 private:
  cocos2d::PhysicsBody* head_;
  cocos2d::PhysicsBody* wheel_;
  cocos2d::PhysicsBody* wheel2_;
  cocos2d::PhysicsBody* body_;

  PhysicsBody* floor_;
  
  float maxVelocity_;
  float baseForce_;
  float rotation_force_;

  bool right_wheel_touching_, left_wheel_touching_;

};

#endif // __CAR_H__
