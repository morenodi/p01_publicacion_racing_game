#include "end_flag.h"

#include "Physics/physics.hpp"

EndFlag::EndFlag() {
	Pickup::Pickup();
	EndFlag::setTag(end_flag_tag);
}

EndFlag::EndFlag(Point *surface, float distance) {
	Pickup::Pickup(surface, distance);
}

bool EndFlag::init() {
	if (!Pickup::init()) {
		return false;
	}

	// enables physics collision
	auto contactListener = EventListenerPhysicsContact::create();
	contactListener->onContactBegin = CC_CALLBACK_1(EndFlag::onContactBegin, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(contactListener, this);

	return true;
}

void EndFlag::set_image(const std::string &filename) {
	image_ = Sprite::create(filename);
	image_->setContentSize(getContentSize());
	image_->setAnchorPoint(Vec2(0.0f, 0.0f));
 //image_->setScaleY(-1.0f);

	auto physicsBody = PhysicsBody::createBox(image_->getContentSize() * 1.1f + Size(0.0f, 5000.0f), PhysicsMaterial(0.1f, 1.0f, 0.0f));
	physicsBody->setDynamic(false);
	physicsBody->setCategoryBitmask(CATEGORY_FLAG);
	physicsBody->setCollisionBitmask(COLLISION_DEFAULT);
	physicsBody->setContactTestBitmask(OVERLAP_FLAG);
	image_->addComponent(physicsBody);
	addChild(image_);
}


void EndFlag::update(float dt) {}

bool EndFlag::onContactBegin(PhysicsContact& contact) {
	auto bodyA = contact.getShapeA()->getBody();
	auto bodyB = contact.getShapeB()->getBody();

	return true;
}