#pragma once

#include "cocos2d.h"
#include "Pickup\pickup.h"

#define end_flag_tag 15

USING_NS_CC;

class EndFlag : public Pickup
{
public:
	EndFlag();
	/*
	\brief Constructor with the info needed to place the pick up
	\param surface Origin point and end point of the plane that is going to be placed.
	\param distance How high of the surface is.
	*/
	EndFlag(Point *surface, float distance);
	virtual bool init();

	virtual ~EndFlag() {}
	
	virtual void update(float dt);
	bool onContactBegin(PhysicsContact& contact);

	// Getters and setters
	void set_image(const std::string &filename);

	CREATE_FUNC(EndFlag);
private:
	Sprite *image_;
};
