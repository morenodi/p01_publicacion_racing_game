#pragma once

#include "cocos2d.h"

USING_NS_CC;

class Pickup : public Node
{
public:
	Pickup();

	/*
	\brief Constructor with the info needed to place the pick up
	\param surface Origin point and end point of the plane that is going to be placed.
	\param distance How high of the surface is.
	*/
	Pickup(Point *surface, float distance);
	virtual ~Pickup() {}

	virtual bool init();
	virtual void update(float dt) {}

	/*
	\brief Place the object in the world.
	*/
	void place();

	// getter and setters
	/*
	\param surface Origin point and end point of the plane that is going to be placed.
	*/
	void set_surface(Point surface[]) { surface_ = surface; }
	
	/*
	\param distance How high of the surface is.
	*/
	void set_distance(float distance) { distance_to_surface_ = distance; }

	CREATE_FUNC(Pickup);

private:


	Point *surface_;
	float distance_to_surface_;
	
};
