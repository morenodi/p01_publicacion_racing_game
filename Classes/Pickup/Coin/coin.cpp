#include "coin.h"

#include "Physics/physics.hpp"

Coin::Coin() {
	Pickup::Pickup();
	Node::setTag(coin_tag);
}

Coin::Coin(Point *surface, float distance, unsigned int value) {
	Pickup::Pickup(surface, distance);
	value_ = value;
}

bool Coin::init() {
	if (!Pickup::init()) {
		return false;
	}

	return true;
}

void Coin::set_image(const std::string &filename) {
  image_ = Sprite::create(filename);
  image_->setContentSize(getContentSize());
  auto physicsBody = PhysicsBody::createBox(image_->getContentSize() * 1.1f, PhysicsMaterial(0.1f, 1.0f, 0.0f));
  physicsBody->setDynamic(false);
  physicsBody->setCategoryBitmask(CATEGORY_COIN);
  physicsBody->setCollisionBitmask(COLLISION_DEFAULT);
  physicsBody->setContactTestBitmask(OVERLAP_COIN);
  image_->addComponent(physicsBody);
  addChild(image_);
}

