#pragma once

#include "cocos2d.h"
#include "Pickup\pickup.h"

#define coin_tag 10

USING_NS_CC;

class Coin : public Pickup
{
public:
	Coin();
	/*
	\brief Constructor with the info needed to place the pick up
	\param surface Origin point and end point of the plane that is going to be placed.
	\param distance How high of the surface is.
	*/
	Coin(Point *surface, float distance, unsigned int value);
	virtual bool init();

	virtual ~Coin() {}
	
	bool onContactBegin(PhysicsContact& contact);

	// Getters and setters
	void set_image(const std::string &filename);
	void set_value(unsigned int value) { value = value_; }
	unsigned int value() { return value_; }

	CREATE_FUNC(Coin);
private:
	Sprite *image_;
	float value_;
};
