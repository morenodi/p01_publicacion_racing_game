#include "pickup.h"

Pickup::Pickup() {
	surface_ = nullptr;
	distance_to_surface_ = 0;
}

Pickup::Pickup(Point *surface, float distance) {
	surface_ = surface;
	distance_to_surface_ = distance;
}

bool Pickup::init() {
	if (!Node::init()) {
		return false;
	}

	return true;
}

void Pickup::place() {
	Point new_point = Point::ZERO;
	if (surface_ != nullptr) {

		Point vector_surface = surface_[1] - surface_[0];
		Point normal = Point(-vector_surface.y, vector_surface.x);
		normal = normal.getNormalized();
		Point middle_surface = surface_[0] + vector_surface / 2;

		new_point = middle_surface + normal * distance_to_surface_;
		float angle = 180 * atan2(normal.x, normal.y) / 3.141592653f;
		Node::setRotation(angle);
	}

	Node::setPosition(new_point);
}


