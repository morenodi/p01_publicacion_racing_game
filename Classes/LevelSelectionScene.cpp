#include "LevelSelectionScene.h"
#include "AppDelegate.h"
#include "SimpleAudioEngine.h"
#include "ui\CocosGUI.h"

USING_NS_CC;

Scene* LevelSelectionScene::createScene() {
  auto scene = Scene::create();
  auto layer = LevelSelectionScene::create();
  scene->addChild(layer);

  return scene;
}

bool LevelSelectionScene::init() {
  if (!Layer::init()) {
    return false;
  }

  auto visibleSize = Director::getInstance()->getVisibleSize();
  Vec2 origin = Director::getInstance()->getVisibleOrigin();

  background1 = Sprite::create("background.jpg");
  background2 = Sprite::create("background.jpg");
  background3 = Sprite::create("background.jpg");

  background1->setAnchorPoint(Point(0, 0));
  background2->setAnchorPoint(Point(0, 0));
  background3->setAnchorPoint(Point(0, 0));

  background1->setScale(visibleSize.width / background1->getContentSize().width,
	  visibleSize.height / background1->getContentSize().height);
  background2->setScale(visibleSize.width / background2->getContentSize().width,
	  visibleSize.height / background2->getContentSize().height);
  background3->setScale(visibleSize.width / background2->getContentSize().width,
	  visibleSize.height / background2->getContentSize().height);

  background1->setPosition(Point(0, 0));
  background2->setPosition(Point(visibleSize.width, 0));
  background3->setPosition(Point(2 * visibleSize.width, 0));

  this->addChild(background1, 0);
  this->addChild(background2, 0);
  this->addChild(background3, 0);

  auto scroll_1 = RepeatForever::create(MoveBy::create(5.0f, Point(-visibleSize.width, 0)));
  background1->runAction(scroll_1);
  auto scroll_2 = RepeatForever::create(MoveBy::create(5.0f, Point(-visibleSize.width, 0)));
  background2->runAction(scroll_2);
  auto scroll_3 = RepeatForever::create(MoveBy::create(5.0f, Point(-visibleSize.width, 0)));
  background3->runAction(scroll_3);

  this->scheduleUpdate();

  /*
  auto bAccelerate = ui::Button::create("gasandbrake.png", "gasandbrake.png");
  bAccelerate->setPosition(Point(70.0f, 40.0f));
  bAccelerate->setScale(0.3);
  bAccelerate->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
	  switch (type)
	  {
	  case ui::Widget::TouchEventType::BEGAN:
		  break;
	  case ui::Widget::TouchEventType::ENDED:
		  break;
	  default:
		  break;
	  }
  });
  this->addChild(bAccelerate);
 
  auto bBrake = ui::Button::create("gasandbrake.png", "gasandbrake.png");
  bBrake->setPosition(Point(visibleSize.width - 70.0f, 40.0f));
  bBrake->setScale(0.3);
  bBrake->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
	  switch (type)
	  {
	  case ui::Widget::TouchEventType::BEGAN:
		  break;
	  case ui::Widget::TouchEventType::ENDED:
		  break;
	  default:
		  break;
	  }
  });
  this->addChild(bBrake);

  auto tPosition = Label::create("1/4", "fonts/SF Comic Script.ttf", 40);
  tPosition->setTextColor(Color4B(0,0,0,255));
  tPosition->setPosition(Point(100.0f, visibleSize.height - 30.0f));
  tPosition->setHorizontalAlignment(kCCTextAlignmentLeft);
  this->addChild(tPosition);

  auto sPosition = Sprite::create("end_flag.png");
  sPosition->setPosition(Point(30.0f, visibleSize.height - 30.0f));
  sPosition->setScale(0.15f);
  sPosition->setRotation3D(Vec3(15.0f, 180.0f, 20.0f));
  this->addChild(sPosition);

  auto tCoins = Label::create("25", "fonts/SF Comic Script.ttf", 40);
  tCoins->setTextColor(Color4B(0, 0, 0, 255));
  tCoins->setPosition(Point(90.0f, visibleSize.height - 80.0f));
  tCoins->setHorizontalAlignment(kCCTextAlignmentLeft);
  this->addChild(tCoins);

  auto sCoins = Sprite::create("coin.png");
  sCoins->setPosition(Point(35.0f, visibleSize.height - 80.0f));
  sCoins->setScale(0.15f);
  this->addChild(sCoins);

  auto tTimer = Label::create("30.84", "fonts/SF Comic Script.ttf", 40);
  tTimer->setTextColor(Color4B(0, 0, 0, 255));
  tTimer->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height - 30.0f));
  this->addChild(tTimer);
  */
  
  auto selectLevel = Sprite::create("UI/tSelectLevel.png");
  selectLevel->setPosition(Point(-2 * visibleSize.width / 2 + origin.x, visibleSize.height / 2 + 270.0f));
  selectLevel->setScale(0.6);
  this->addChild(selectLevel);

  Vec2 offset = { 210, 150 };

  auto pageHills = Sprite::create("UI/tHills.png");
  pageHills->setScale(0.5);
  pageHills->setPosition(120,50);

  auto layout1 = ui::Layout::create();
  layout1->setContentSize(Size(1280, 720));
  layout1->addChild(pageHills);

  char stars_text[20] = "Total stars: ";
  char num_stars[20];
  sprintf(num_stars, "%d", AppDelegate::instance().get_stars());
  strcat(stars_text, num_stars);
  auto stars = Label::create(stars_text, "fonts/SF Comic Script.ttf", 65);
  selectLevel->addChild(stars);
  stars->setPosition(Point(-360.0f, 120.0f));
  stars->setTextColor(Color4B(79, 34, 2, 255));

  ui::Button *bLevelHills[15];
  for (int x = 0; x < 5; x++) {
	  for (int y = 0; y < 3; y++) {
		  int i = 5 * y + x;
		  bLevelHills[i] = ui::Button::create("UI/itemGrass.png", "UI/itemGrass_s.png");
		  bLevelHills[i]->setTitleFontName("fonts/numbers.ttf");
		  bLevelHills[i]->setTitleText(std::to_string(i+1));
		  bLevelHills[i]->setTitleFontSize(50);
		  bLevelHills[i]->setTitleColor(Color3B(79,34,2));
		  bLevelHills[i]->setPosition(Point(offset.x + x * offset.x, 500 - y * offset.y));
		  bLevelHills[i]->setScale(0.0f);

		  layout1->addChild(bLevelHills[i]);

		  auto action = Sequence::create(DelayTime::create(0.2f * (float)i), EaseBackOut::create(ScaleTo::create(0.5f, 1.0f)), nullptr);
		  bLevelHills[i]->runAction(action); 
	  }
  }

  bLevelHills[0]->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {switch (type) { case ui::Widget::TouchEventType::ENDED:AppDelegate::instance().load_scene(SceneID::kSceneID_Race, 0); break; default:break; }});
  bLevelHills[1]->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {switch (type) { case ui::Widget::TouchEventType::ENDED:AppDelegate::instance().load_scene(SceneID::kSceneID_Race, 1); break; default:break; }});
  bLevelHills[2]->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {switch (type) { case ui::Widget::TouchEventType::ENDED:AppDelegate::instance().load_scene(SceneID::kSceneID_Race, 2); break; default:break; }});
  bLevelHills[3]->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {switch (type) { case ui::Widget::TouchEventType::ENDED:AppDelegate::instance().load_scene(SceneID::kSceneID_Race, 3); break; default:break; }});
  bLevelHills[4]->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {switch (type) { case ui::Widget::TouchEventType::ENDED:AppDelegate::instance().load_scene(SceneID::kSceneID_Race, 4); break; default:break; }});
  bLevelHills[5]->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {switch (type) { case ui::Widget::TouchEventType::ENDED:AppDelegate::instance().load_scene(SceneID::kSceneID_Race, 5); break; default:break; }});
  bLevelHills[6]->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {switch (type) { case ui::Widget::TouchEventType::ENDED:AppDelegate::instance().load_scene(SceneID::kSceneID_Race, 6); break; default:break; }});
  bLevelHills[7]->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {switch (type) { case ui::Widget::TouchEventType::ENDED:AppDelegate::instance().load_scene(SceneID::kSceneID_Race, 7); break; default:break; }});
  bLevelHills[8]->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {switch (type) { case ui::Widget::TouchEventType::ENDED:AppDelegate::instance().load_scene(SceneID::kSceneID_Race, 8); break; default:break; }});
  bLevelHills[9]->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {switch (type) { case ui::Widget::TouchEventType::ENDED:AppDelegate::instance().load_scene(SceneID::kSceneID_Race, 9); break; default:break; }});
  bLevelHills[10]->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {switch (type) { case ui::Widget::TouchEventType::ENDED:AppDelegate::instance().load_scene(SceneID::kSceneID_Race, 10); break; default:break; }});
  bLevelHills[11]->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {switch (type) { case ui::Widget::TouchEventType::ENDED:AppDelegate::instance().load_scene(SceneID::kSceneID_Race, 11); break; default:break; }});
  bLevelHills[12]->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {switch (type) { case ui::Widget::TouchEventType::ENDED:AppDelegate::instance().load_scene(SceneID::kSceneID_Race, 12); break; default:break; }});
  bLevelHills[13]->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {switch (type) { case ui::Widget::TouchEventType::ENDED:AppDelegate::instance().load_scene(SceneID::kSceneID_Race, 13); break; default:break; }});
  bLevelHills[14]->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {switch (type) { case ui::Widget::TouchEventType::ENDED:AppDelegate::instance().load_scene(SceneID::kSceneID_Race, 14); break; default:break; }});

  auto pageDesert = Sprite::create("UI/tHills.png");
  pageDesert->setScale(0.5);
  pageDesert->setPosition(120, 50);

  auto layout2 = ui::Layout::create();
  layout2->setContentSize(Size(1280, 720));
  layout2->addChild(pageDesert);

  ui::Button *bLevelDesert[15];
  for (int x = 0; x < 5; x++) {
	  for (int y = 0; y < 3; y++) {
		  int i = 5 * y + x;
		  bLevelDesert[i] = ui::Button::create("UI/WoodItem.png", "UI/WoodItem.png");
		  bLevelDesert[i]->setTitleFontName("fonts/numbers.ttf");
		  bLevelDesert[i]->setTitleText(std::to_string(i));
		  bLevelDesert[i]->setTitleFontSize(50);
		  bLevelDesert[i]->setTitleColor(Color3B(79, 34, 2));
		  bLevelDesert[i]->setPosition(Point(offset.x + x * offset.x, 500 - y * offset.y));
		  bLevelDesert[i]->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
			  switch (type)
			  {
			  case ui::Widget::TouchEventType::BEGAN:
				  break;
			  case ui::Widget::TouchEventType::ENDED:
				  break;
			  default:
				  break;
			  }
		  });
		  layout2->addChild(bLevelDesert[i]);
	  }
  }

  auto levelPageView = ui::PageView::create();
  levelPageView->setTouchEnabled(true);
  levelPageView->setContentSize(Size(1280, 720));
  levelPageView->setPosition(Point(2000,0));

  levelPageView->addPage(layout1);
  levelPageView->addPage(layout2);
  this->addChild(levelPageView);

  auto soundDisabled = ui::CheckBox::create("UI/soundYes.png", "UI/soundNo.png");
  soundDisabled->setPosition(Point(visibleSize.width - 45, 40));
  soundDisabled->setScale(0.1f);
  soundDisabled->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
	  switch (type)
	  {
	  case ui::Widget::TouchEventType::BEGAN:
		  //DO
		  break;
	  case ui::Widget::TouchEventType::ENDED:
		  break;
	  default:
		  break;
	  }
  });
  this->addChild(soundDisabled);

  auto bBack = ui::Button::create("UI/tBack.png", "UI/tBack.png");
  bBack->setPosition(Point(visibleSize.width - 80.0f, visibleSize.height - 50.0f));
  bBack->setScale(0.4f);
  bBack->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
	  switch (type)
	  {
	  case ui::Widget::TouchEventType::BEGAN:
		  load_scene(SceneID::kSceneID_Main);
		  break;
	  case ui::Widget::TouchEventType::ENDED:
		  break;
	  default:
		  break;
	  }
  });
  this->addChild(bBack);

  // - Actions
  auto titleIntoScreen = EaseBackOut::create(MoveTo::create(0.5f, Point(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + 270.0f)));
  auto levelsIntoScreen = EaseBackOut::create(MoveTo::create(0.5f, Point(0,0)));
  selectLevel->runAction(titleIntoScreen);
  levelPageView->runAction(levelsIntoScreen);
  
  /* Pause Menu
  auto bgAlpha = Sprite::create("UI/AlphaGrey.png");
  bgAlpha->setAnchorPoint(Point(0, 0));
  this->addChild(bgAlpha);

  auto woodPanel = Sprite::create("UI/WoodPanel.png");
  woodPanel->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
  //woodPanel->setScale(0.5f, 1.0f);
  this->addChild(woodPanel);

  auto bResume = ui::Button::create("UI/tResume.png", "UI/tResume_s.png");
  bResume->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y - 0.0f));
  bResume->setScale(0.75f);
  bResume->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
	  switch (type)
	  {
	  case ui::Widget::TouchEventType::BEGAN:
		  break;
	  case ui::Widget::TouchEventType::ENDED:
		  
		  break;
	  default:
		  break;
	  }
  });
  this->addChild(bResume);

  auto bQuitGame = ui::Button::create("UI/tQuitGame.png", "UI/tQuitGame_s.png");
  bQuitGame->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y - 140.0f));
  bQuitGame->setScale(0.75f);
  bQuitGame->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
	  switch (type)
	  {
	  case ui::Widget::TouchEventType::BEGAN:
		  break;
	  case ui::Widget::TouchEventType::ENDED:

		  break;
	  default:
		  break;
	  }
  });
  this->addChild(bQuitGame);

  auto soundDisabled = ui::CheckBox::create("UI/soundYes.png", "UI/soundNo.png");
  soundDisabled->setPosition(Point(visibleSize.width - 45, 40));
  soundDisabled->setScale(0.1f);
  soundDisabled->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
	  switch (type)
	  {
	  case ui::Widget::TouchEventType::BEGAN:
		  //DO
		  break;
	  case ui::Widget::TouchEventType::ENDED:
		  break;
	  default:
		  break;
	  }
  });
  this->addChild(soundDisabled);
  */
  return true;
}

void LevelSelectionScene::update(float dt) {
	auto visibleSize = Director::getInstance()->getVisibleSize();

	if (background1->getPosition().x <= -visibleSize.width) {
		background1->setPosition(background3->getPosition().x + visibleSize.width, 0);
	}
	if (background2->getPosition().x <= -visibleSize.width) {
		background2->setPosition(background1->getPosition().x + visibleSize.width, 0);
	}
	if (background3->getPosition().x <= -visibleSize.width) {
		background3->setPosition(background2->getPosition().x + visibleSize.width, 0);
	}
}

void LevelSelectionScene::load_scene(SceneID scene) {
  AppDelegate::instance().load_scene(scene);
}