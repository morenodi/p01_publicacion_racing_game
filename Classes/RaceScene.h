#ifndef __RACE_SCENE_H__
#define __RACE_SCENE_H__

#include "cocos2d.h"
#include "Hills/Hills.h"
#include "Pickup/Coin/coin.h"
#include "Parallax/parallax.h"
#include "Car/car.h"
#include "AppDelegate.h"

#include <vector>

USING_NS_CC;

typedef struct {
  float time;
  int coins;
  int position;
  int total_players;
} LevelParams;


class RaceScene : public Layer {
 public:
  RaceScene();
  ~RaceScene();

  static cocos2d::Scene* createScene();

  virtual bool init();
  void update(float delta_time);
    
  // a selector callback
  void menuCloseCallback(cocos2d::Ref* pSender);

  virtual bool onTouchBegan(cocos2d::Touch*, cocos2d::Event*);
  virtual void onTouchEnded(cocos2d::Touch*, cocos2d::Event*);

  // implement the "static create()" method manually
  CREATE_FUNC(RaceScene);

  void onEnter();

  void set_level_params(LevelParams *params);
  void set_level_id(int id) { scene_id_ = id; }

  bool onContactBegin(PhysicsContact& contact);

 protected:
  void generate_terrain();
  void generate_parallax();
  void create_HUD();
  void create_pause();
  void generate_car();
  void generate_bots();

  void update_countdown(float delta_time);
  void update_car(float delta_time);
  void update_bot(Car *car);
  void update_camera(float delta_time);
  void update_timer(float delta_time);
  void update_coins();
  void update_position();

  void coin_tweening(Node *node);
  void check_position();
  void end_race();
  void pause();

  void load_scene(SceneID scene);

  LevelParams params_;
  cocos2d::Node *HUD_;
  cocos2d::Node *pause_menu_;
  Camera *myCamera_;
  PhysicsWorld *world_;
  Hills *hills_;
  Parallax *parallax_;
  std::vector<Car *> bots_;
  Car *car_;
  Size visibleSize_;
  Vec2 origin_;
  float timer_, counter_;
  int stars_;
  int coins_, position_;
  int scene_id_;

  bool acelerate_, brake_;
  bool race_ended_;
  bool is_paused_;

  int truck_sound_effect_id_;
  bool first_acelerate_;
  bool first_decelerate_;
	
};

#endif // __RACE_SCENE_H__
