#include "MainMenuScene.h"
#include "AppDelegate.h"
#include "SimpleAudioEngine.h"
#include "ui\CocosGUI.h"

USING_NS_CC;

Scene* MainMenuScene::createScene() {
	auto scene = Scene::create();
	auto layer = MainMenuScene::create();
	scene->addChild(layer);

	return scene;
}

bool MainMenuScene::init() {
	if (!Layer::init()) {
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	
	background1 = Sprite::create("background.jpg");
	background2 = Sprite::create("background.jpg");
	background3 = Sprite::create("background.jpg");

	background1->setAnchorPoint(Point(0, 0));
	background2->setAnchorPoint(Point(0, 0));
	background3->setAnchorPoint(Point(0, 0));

	background1->setScale(visibleSize.width / background1->getContentSize().width,
		visibleSize.height / background1->getContentSize().height);
	background2->setScale(visibleSize.width / background2->getContentSize().width,
		visibleSize.height / background2->getContentSize().height);
	background3->setScale(visibleSize.width / background2->getContentSize().width,
		visibleSize.height / background2->getContentSize().height);

	background1->setPosition(Point(0, 0));
	background2->setPosition(Point(visibleSize.width, 0));
	background3->setPosition(Point(2 * visibleSize.width, 0));

	this->addChild(background1, 0);
	this->addChild(background2, 0);
	this->addChild(background3, 0);

	auto scroll_1 = RepeatForever::create(MoveBy::create(5.0f, Point(-visibleSize.width, 0)));
	background1->runAction(scroll_1);
	auto scroll_2 = RepeatForever::create(MoveBy::create(5.0f, Point(-visibleSize.width, 0)));
	background2->runAction(scroll_2);
	auto scroll_3 = RepeatForever::create(MoveBy::create(5.0f, Point(-visibleSize.width, 0)));
	background3->runAction(scroll_3);

	this->scheduleUpdate();

	cocos2d::Sprite* effect;
	effect = Sprite::create("UI/effect_1024_01.png");
	effect->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y + 120.0f));
	effect->setScale(0.02f);
	this->addChild(effect);

	auto rotateForever = RepeatForever::create(RotateBy::create(5.0f, 360));
	auto scaleToScreen = ScaleBy::create(0.4f, 35.0f);
	auto easeScaleEffect = EaseBackOut::create(scaleToScreen->clone());
	effect->runAction(rotateForever);
	effect->runAction(easeScaleEffect);

	auto bPlay = ui::Button::create("UI/tPlay.png", "UI/tPlay_s.png");
	bPlay->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y - 80.0f));
	bPlay->setScale(0.5f);
	bPlay->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
      playClickSound();
			load_scene(SceneID::kSceneID_LevelSelection);
			break;
		default:
			break;
		}
	});
	this->addChild(bPlay);

	auto bShop = ui::Button::create("UI/tShop.png", "UI/tShop_s.png");
	bShop->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y - 180.0f));
	bShop->setScale(0.5f);
	bShop->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			//DO
			break;
		case ui::Widget::TouchEventType::ENDED:
      playClickSound();
			break;
		default:
			break;
		}
	});
	this->addChild(bShop);

	auto bCredits = ui::Button::create("UI/tCredits.png", "UI/tCredits_s.png");
	bCredits->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y - 280.0f));
	bCredits->setScale(0.5f);
	bCredits->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			//DO
			break;
		case ui::Widget::TouchEventType::ENDED:
      playClickSound();
			break;
		default:
			break;
		}
	});
	this->addChild(bCredits);

	cocos2d::Sprite* logo;
	logo = Sprite::create("UI/tLogo.png");
	logo->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y + 120.0f));
	logo->setScale(0.025f);
	this->addChild(logo);

	auto easeScaleLogo = EaseBackOut::create(scaleToScreen->clone());
	logo->runAction(easeScaleLogo);

	auto soundDisabled = ui::CheckBox::create("UI/soundYes.png", "UI/soundNo.png");
  soundDisabled->setSelected(!AppDelegate::instance().is_muted());
	soundDisabled->setPosition(Point(visibleSize.width - 45, 40));
	soundDisabled->setScale(0.1f);
	soundDisabled->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
      playClickSound();
      AppDelegate::instance().mute();
			break;
		default:
			break;
		}
	});
	this->addChild(soundDisabled);

	auto bExit = ui::Button::create("UI/tExit.png", "UI/tExit.png");
	bExit->setPosition(Point(visibleSize.width - 80.0f, visibleSize.height - 50.0f));
	bExit->setScale(0.4f);
	bExit->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
      playClickSound();
			Director::getInstance()->end();
			break;
		case ui::Widget::TouchEventType::ENDED:
			break;
		default:
			break;
		}
	});
	this->addChild(bExit);


  auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
  if (!audio->isBackgroundMusicPlaying())
    audio->playBackgroundMusic("Cathy-Akiko.wav", true);

	return true;
}

void MainMenuScene::playClickSound() {

  auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
  audio->playEffect("click3.wav");
}

void MainMenuScene::update(float dt) {
	auto visibleSize = Director::getInstance()->getVisibleSize();

	if (background1->getPosition().x <= -visibleSize.width) {
		background1->setPosition(background3->getPosition().x + visibleSize.width, 0);
	}
	if (background2->getPosition().x <= -visibleSize.width) {
		background2->setPosition(background1->getPosition().x + visibleSize.width, 0);
	}
	if (background3->getPosition().x <= -visibleSize.width) {
		background3->setPosition(background2->getPosition().x + visibleSize.width, 0);
	}
}

void MainMenuScene::load_scene(SceneID scene) {
	AppDelegate::instance().load_scene(scene);
}