#ifndef  _APP_DELEGATE_H_
#define  _APP_DELEGATE_H_

#include "cocos2d.h"

/**
@brief    The cocos2d Application.

Private inheritance here hides part of interface from Director.
*/

typedef enum {
  kSceneID_Main = 0,
  kSceneID_LevelSelection,
  kSceneID_Race
} SceneID;

struct DataDisk{
  int total_stars_;

};

class  AppDelegate : private cocos2d::Application {
 public:

  static AppDelegate& instance() {
    static AppDelegate *inst_ = new AppDelegate();
    return *inst_;
  }

  virtual void initGLContextAttrs();

  /**
  @brief    Implement Director and Scene init code here.
  @return true    Initialize success, app continue.
  @return false   Initialize failed, app terminate.
  */
  virtual bool applicationDidFinishLaunching();

  /**
  @brief  Called when the application moves to the background
  @param  the pointer of the application
  */
  virtual void applicationDidEnterBackground();

  /**
  @brief  Called when the application reenters the foreground
  @param  the pointer of the application
  */
  virtual void applicationWillEnterForeground();


  /*
    scene = type of scene
    id = only used in RaceScene to identify the level
  */
  void load_scene(SceneID scene, int id = 0);

  void add_stars(int number);
  int get_stars();

  /*
  Saves on disk
  */
  void saveOnDisk();
  /*
  Load from disk
  */
  void loadFromDisk();

  DataDisk getData() { return data_; }
  int getActualId() { return actual_scene_id_; }

  void mute();
  bool is_muted() { return is_muted_; }

  // Methods to block
  AppDelegate(const AppDelegate& arg) = delete; // Copy CTR
  AppDelegate(const AppDelegate&& arg) = delete;  // Move CTR
  AppDelegate& operator=(const AppDelegate& arg) = delete; // Assignment operator
  AppDelegate& operator=(const AppDelegate&& arg) = delete; // Move operator

 private:
   AppDelegate();
   virtual ~AppDelegate();

   DataDisk data_;

   int total_stars_ = 0;
   int actual_scene_id_;

   bool is_muted_;

};

#endif // _APP_DELEGATE_H_

