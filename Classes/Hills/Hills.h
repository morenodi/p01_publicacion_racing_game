#ifndef __HILLS_H__
#define __HILLS_H__ 1

#include "Pickup\Coin\coin.h"
#include "Pickup\End_Flag\end_flag.h"
#include "Parallax\parallax.h"
#include "cocos2d.h"

USING_NS_CC;

#define MaxPhysicsVertices 1000
#define MaxDrawableVertices 2000

class Hills : public Node {
public:
	Hills();
	~Hills();

	virtual bool init();
	/** What is going to be draw */
	void onDraw(const Mat4 &transform);

	/** 
	\brief Creates the terrain of the game.
	\param origin Point in where it will start
	\param verts Total of points
	\param height Height of the road
	\param variance Max variance of height
	\param distance_per_point How much distance will be per point
	\param loops How smooth will the curve be
	*/
	void createHills(
		const Vec2 &origin,
		unsigned int verts,
		float height,
		float variance,
		float distance_per_point, 
		int loops);

	/**
	It uses the verts from createHills and it creates a mesh with quad bezier
	\param segments How many points there will be per segment
	*/
	void generateMesh(int segments);
	void generateMeshSegments(int index, Vec2 p);

	// Coins
	void generateCoins(int verts_between_coins, std::string sprite_file, float size);
		
	Point getIndexedPoint(int index) { return vertices_smooth_[index]; }


	virtual void draw(Renderer *renderer, const Mat4& transform, uint32_t flags);
	virtual void update(float dt);

	void set_grass_texture(Sprite* grassSprite);
	void set_shade_texture(Sprite* shadeSprite);
	void set_ground_texture(Sprite* groundSprite);
	
	CustomCommand _renderCmds[1];


	// getters and setters
	inline float distance() { return distance_; }
	inline int vertices_count() { return vertices_count_; }
	
	CREATE_FUNC(Hills);
private:
	// EndFlag
	void generateEndFlag(int last_vert, std::string sprite_file, float size);
	
	Vec2* vertices_;
	Vec2* vertices_smooth_;

	float distance_;
	int vertices_count_;
	int segments_;
	Texture2D* _grassTexture;
	Texture2D* _shadeTexture;
	Texture2D* _groundTexture;
	Point _grassVertices[20000];
	Point _grassTexCoords[20000];
	Point _shadeVertices[20000];
	Point _shadeTexCoords[20000];
	Point _groundVertices[20000];
	Point _groundTexCoords[20000];

};

#endif // __HILLS_H__
