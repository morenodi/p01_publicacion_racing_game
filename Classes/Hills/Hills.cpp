
#include "Hills.h"
#include "Random\Random.h"

#include "Physics/physics.hpp"

#define RANDOM(X) Random::next()%(int)X
#define RANDOM_SEED(X) Random::setSeed(X)

Hills::Hills() {
  Sprite * grassSprite = Sprite::create("grass.png");
  set_grass_texture(grassSprite);

  Sprite * shadeSprite = Sprite::create("shade.png");
  set_shade_texture(shadeSprite);

  Sprite * groundSprite = Sprite::create("ground.png");
  set_ground_texture(groundSprite);

  GLProgram *shaderProgram_ = ShaderCache::getInstance()->getGLProgram(GLProgram::SHADER_NAME_POSITION_TEXTURE);
  setGLProgram(shaderProgram_);
  scheduleUpdate();
  vertices_ = 0;
}

Hills::~Hills() {
	CC_SAFE_DELETE_ARRAY(vertices_smooth_);
}

static Vec2 generatePoint(const Vec2 &previous, float distance, float heigth,
                          float variance, int index, int total_points) {
	Vec2 new_point = Vec2(previous.x + distance * (1.25 - 0.75 * (float)index/(float)total_points),
		  (RANDOM(variance) - variance /1.25f) * ((float)index/((float)total_points/1.25f)) + heigth);

	if (new_point.y < 75.0f)
		new_point.y = 75.0f;
	return new_point;
}

void Hills::generateMeshSegments(int i, Vec2 p) {
  i *= 2;
  // grass
  _grassVertices[i] = Point(p.x, p.y - 12.0f);
  _grassTexCoords[i] = Point(p.x / _grassTexture->getPixelsWide(), 1.0f);
  _grassVertices[i + 1] = Point(p.x, p.y);
  _grassTexCoords[i + 1] = Point(p.x / _grassTexture->getPixelsWide(), 0);

  // shade

  _shadeVertices[i] = Point(p.x, p.y - 16.0f);
  _shadeTexCoords[i] = Point(p.x / _shadeTexture->getPixelsWide(), 1.0f);
  _shadeVertices[i + 1] = Point(p.x, p.y);
  _shadeTexCoords[i + 1] = Point(p.x / _shadeTexture->getPixelsWide(), 0);

  // ground

  _groundVertices[i] = Point(p.x, - 1000.0f);
  _groundTexCoords[i] = Point(p.x / _groundTexture->getPixelsWide(), 1.0f);
  _groundVertices[i + 1] = Point(p.x, p.y);
  _groundTexCoords[i + 1] = Point(p.x / _groundTexture->getPixelsWide(), 0);
}

void Hills::generateMesh(int segments) {
  segments_ = (segments + 1) * (vertices_count_ - 2) + 5 - vertices_count_;
  Vec2* vertices_mesh = new (std::nothrow) Vec2[segments_];

  Point origin, destination, control;

  float t = 0.0f;
  int v = 0;
  vertices_mesh[v] = vertices_[0];
  generateMeshSegments(v, vertices_mesh[v]);
  v++;

  for (int i = 1; i < vertices_count_; i++) {
  control = vertices_[i];
  origin = vertices_[i] + Point(vertices_[i], vertices_[i - 1]) / 2.0f;
  destination = vertices_[i] + Point(vertices_[i], vertices_[i + 1]) / 2.0f;

  vertices_mesh[v] = origin;
  generateMeshSegments(v, vertices_mesh[v]);
  v++;

  t = 1.0f / segments;
  if (i == vertices_count_ - 1) break;
  for (int j = 1; j < segments; j++) {
		  vertices_mesh[v].x = powf(1 - t, 2) * origin.x + 2.0f * (1 - t) * t * control.x + t * t * destination.x;
		  vertices_mesh[v].y = powf(1 - t, 2) * origin.y + 2.0f * (1 - t) * t * control.y + t * t * destination.y;
		  generateMeshSegments(v, vertices_mesh[v]);
		  v++;

		  t += 1.0f / segments;
  }
  /*
  vertices_mesh[v] = destination;
  generateMeshSegments(v, vertices_mesh[v]);
  v++;
  */
  }

  //vertices_mesh[v++] = vertices_[vertices_count_] + Point(vertices_[vertices_count_], vertices_[vertices_count_-1]) / 2.0f;
  vertices_mesh[v] = vertices_[vertices_count_ - 1];
  generateMeshSegments(v, vertices_mesh[v]);
  v++;

  auto physicsBody = PhysicsBody::createEdgeChain(vertices_mesh, segments_, PhysicsMaterial(1.0f, 0.0f, 0.0f));
  physicsBody->setDynamic(false);

  physicsBody->setCategoryBitmask(CATEGORY_HILLS);
  physicsBody->setCollisionBitmask(COLLISION_HILLS);
  physicsBody->setContactTestBitmask(OVERLAP_HILLS);

  addComponent(physicsBody);

  vertices_smooth_ = vertices_mesh;
}

void Hills::createHills(const Vec2 &origin, unsigned int verts, float height,
                        float variance, float distance_per_point, int loops) {
  vertices_ = new (std::nothrow) Vec2[verts];
  if (!vertices_)
  return;

  Point p = origin;
  p.x -= distance_per_point * 10;

  for (unsigned int i = 0; i < 5; i++) {
    vertices_[i] = generatePoint(p, distance_per_point, height * (5 - i + 1), variance, i, verts);
    p = vertices_[i];
  }

  for (unsigned int i = 5; i < verts-10; i++) {
    vertices_[i] = generatePoint(p, distance_per_point, height, variance, i, verts);
    p = vertices_[i];
  }

  for (unsigned int i = verts - 10; i < verts; i++) {
    vertices_[i] = generatePoint(p, distance_per_point * (i + 1 - verts + 10) / 2.0f, height * (i + 1 - verts + 10), variance, i, verts);
    p = vertices_[i];
  }

  vertices_count_ = verts;
  distance_ = distance_per_point;
  generateMesh(loops);
  generateEndFlag( segments_ - (segments_ / (float)verts) * 11, "end_flag.png", Director::getInstance()->getVisibleSize().width * 0.15f);
}

void Hills::generateCoins(int verts_between_coins, std::string sprite_file, float size) {
  for (int i = verts_between_coins; i < segments_ - verts_between_coins; i += verts_between_coins) {

  // Coin creation
  Point p[] = { getIndexedPoint(i), getIndexedPoint(i + 1) };
  Coin *coin = Coin::create();
  coin->setContentSize(Size(size, size));
  coin->set_surface(p);
  coin->set_distance(size * 0.75f);
  coin->place();
  coin->set_image(sprite_file);
  addChild(coin);
  }
}

void Hills::generateEndFlag(int last_vert, std::string sprite_file, float size) {

  Point p[] = { getIndexedPoint(last_vert), getIndexedPoint(last_vert) };
  EndFlag *end_flag = EndFlag::create();
  end_flag->setContentSize(Size(size, size));
  end_flag->set_surface(p);
  end_flag->set_distance(0.0f);
  end_flag->place();
  end_flag->set_image(sprite_file);
  addChild(end_flag);
}

bool Hills::init() {
	if (!Node::init()) {
		return false;
	}

	return true;
}

void Hills::draw(Renderer* renderer, const Mat4 &transform, uint32_t flags) {
	Node::draw(renderer, transform, flags);

	_renderCmds[0].init(0.0f);
	_renderCmds[0].func = CC_CALLBACK_0(Hills::onDraw, this, transform);
	renderer->addCommand(&_renderCmds[0]);
}

void Hills::onDraw(const Mat4 &transform) {
	auto glProgram = getGLProgram();
	glProgram->use();
	glProgram->setUniformsForBuiltins(transform);

	GL::bindTexture2D(_groundTexture->getName());
	GL::enableVertexAttribs(GL::VERTEX_ATTRIB_FLAG_POS_COLOR_TEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_POSITION);
	glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_POSITION, 2, GL_FLOAT, GL_FALSE, 0, _groundVertices);
	glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_TEX_COORD, 2, GL_FLOAT, GL_FALSE, 0, _groundTexCoords);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, (GLsizei)(segments_ * 2));
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	GL::bindTexture2D(_shadeTexture->getName());
	GL::enableVertexAttribs(GL::VERTEX_ATTRIB_FLAG_POS_COLOR_TEX);

	glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_POSITION);
	glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_POSITION, 2, GL_FLOAT, GL_FALSE, 0, _shadeVertices);
	glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_TEX_COORD, 2, GL_FLOAT, GL_FALSE, 0, _shadeTexCoords);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, (GLsizei)(segments_ * 2));
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	GL::bindTexture2D(_grassTexture->getName());
	GL::enableVertexAttribs(GL::VERTEX_ATTRIB_FLAG_POS_COLOR_TEX);

	glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_POSITION);
	glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_POSITION, 2, GL_FLOAT, GL_FALSE, 0, _grassVertices);
	glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_TEX_COORD, 2, GL_FLOAT, GL_FALSE, 0, _grassTexCoords);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, (GLsizei)(segments_ * 2));

}

void Hills::set_grass_texture(Sprite* grassSprite) {
	grassSprite->retain();
	_grassTexture = grassSprite->getTexture();

	// THIS ONLY WORKS IF THE SPRITE IS POW 2
	_grassTexture->setTexParameters({ GL_NEAREST, GL_NEAREST, GL_REPEAT, GL_REPEAT });
}

void Hills::set_shade_texture(Sprite* shadeSprite) {
	shadeSprite->setOpacity(0.2f);
	shadeSprite->retain();
	_shadeTexture = shadeSprite->getTexture();

	// THIS ONLY WORKS IF THE SPRITE IS POW 2
	_shadeTexture->setTexParameters({ GL_NEAREST, GL_NEAREST, GL_REPEAT, GL_REPEAT });
}

void Hills::set_ground_texture(Sprite* groundSprite) {
	groundSprite->retain();
	_groundTexture = groundSprite->getTexture();

	// THIS ONLY WORKS IF THE SPRITE IS POW 2
	_groundTexture->setTexParameters({ GL_NEAREST, GL_NEAREST, GL_REPEAT, GL_REPEAT });
}

void Hills::update(float dt) {
	//lets move the track -3.5 pixel at each update
	//if (abs(getPositionX()) < 7000.0f)
		//setPositionX(getPositionX() - 5.5f);
	
	//parallax_->setPosition(getPosition());
}
