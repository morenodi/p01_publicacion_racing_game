#ifndef __RANDOM_H__
#define __RANDOM_H__ 1

// http://discuss.cocos2d-x.org/t/c-random-with-seed/5845/9

class Random
{
public:
	Random(void);
	Random(long seed);
	static void setSeed(long _seed);
	static int next();
public:
	static long seed;
	static long multiplier;
	static long addend;
	static long mask;
};

#endif